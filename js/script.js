alert("IMANT TESTER");

function funcBefore (){
  //GIF
  $("#information").text("Date waiting...");
}

function funcSuccess (data){
  $("#information").text(data);
}

$(document).ready(function(){
  $("#load").bind("click", function(){
    var admin = "Admin";

    $.ajax ({
      url: "content.php", //на какую страничку переходит
      type: "POST",    //Тип отправления
      data: ({name: admin, number: 5}),   //отправляет инфу
      dataType: "html",   //отправляет
      beforeSend: funcBefore, //пока грузится, выполняем функцию
      success: funcSuccess  //пришел ответ
    });
  })



  $("#done").bind("click", function(){
    $.ajax({
        url: "check.php", //на какую страничку переходит
        type: "POST",    //Тип отправления
        data: ({ name: $("#name").val() }),   //отправляет инфу
        dataType: "html",   //отправляет
        beforeSend: function (){
          $("#information").text("Date waiting...");
        },
        success: function (data){
          if(data == "fail")
            alert("Name occupied");
          else
            alert("Hello "+ $("#name").val() + "!");
        } //пришел ответ
    });
  });
});
